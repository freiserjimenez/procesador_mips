/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mips;

import java.util.Vector;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mips.Cache.ResultadoCache;
import static mips.Instruccion.BEQZ;
import static mips.Instruccion.BNEZ;
import static mips.Instruccion.DADD;
import static mips.Instruccion.DADDI;
import static mips.Instruccion.DDIV;
import static mips.Instruccion.DMUL;
import static mips.Instruccion.DSUB;
import static mips.Instruccion.FIN;
import static mips.Instruccion.JAL;
import static mips.Instruccion.JR;
import static mips.Instruccion.LL;
import static mips.Instruccion.LW;
import static mips.Instruccion.SC;
import static mips.Instruccion.SW;

/**
 *
 * @author
 */
public class Nucleo extends Thread {
	
    private final Cache memCache;
    //Para sincronizar
    //barrera sincroniza la finalizacion de la tarea de cada etapa
    //barrera inicia bloquea a todos los hilos hasta que el padre bloquee los semaforos
    private final CyclicBarrier barrera, barreraInicia;
    //semaforo para checkear si el programa se esta ejecutando
    private final Semaphore mutexEjecutar;
    //sincronizacion entre etapas
    private final Semaphore semIFID, semIDEX, semEXMEM, semMEMWB;
    private static final int retraso = 48;

    private boolean ejecutando; //Bandera que indica si se esta ejecutando un programa MIPS
    private boolean pausa; // bandera que indica si se está ejecutando un hilo, util para pausar el núcleo en los cambios de contexto de hilos
    private int[] r; // conjunto de registros enteros de propósito genereal
    private int rl; // registro especial RL para operaciones LL y SC
    private int direccionLL = 0; //direccion que usara LL para guardar en LR
    private int direccionSC = 0; //direccion que usa SC para comparar con RL
    private boolean falloSC = false; //bandera que se activa si fallo el SC y hay que escribir en RX en WB
    private Vector<Integer> modReg; // Deteccion de conflictos en registros
    private Vector<Integer> modMem; // Conflictos en memoria
    private Mips procesador; // procesador "padre"
    private IF if0;
    private ID id0;
    private EX ex0;
    private MEM mem0;
    private WB wb0;

    //Registros de las etapas
    private int a = 0, b = 0, imm = 0, ao = 0, npc = 0, lmd = 0, contador = 0,  bEX = 0, bMEM = 0, bWB = 0, aEX = 0, aMEM = 0, aWB = 0, aoMEM = 0, aoWB = 0;
    private boolean banderaMEMEX = true, banderaEXID = true, banderaIDIF = true; //true es tiene el paso libre
    private Instruccion irIF, irID, irEX, irMEM, irWB;
    private int pcIF, pcID;
    private int qIF, qID, qEX, qMEM, qWB; // quantum en etapas

    // Contructor
    public Nucleo(Mips p) {
        super("Hilo del Nucleo");
	memCache = new Cache(p.getMemoriaDatos(), 8, 4);
        this.r = new int[32];
        this.modReg = new Vector<Integer>();
        this.modMem = new Vector<Integer>();
        this.procesador = p;
        barrera = new CyclicBarrier(6);
        barreraInicia = new CyclicBarrier(6);
        mutexEjecutar = new Semaphore(1);
        semIFID = new Semaphore(1);
        semIDEX = new Semaphore(1);
        semEXMEM = new Semaphore(1);
        semMEMWB = new Semaphore(1); //WB entra directo
        qIF = qID = qEX = qMEM = qWB = 100; // poner un valor cualquiera mayor que cero para que no se salga desde el principio
        ejecutando = false; //temporal, para usar en debugging
        pausa = true; // cuando se crea el núcleo no debe de haber ningún hilo ejecutandose
        //construccion de los hilos
        if0 = new IF();
        id0 = new ID();
        ex0 = new EX(this.procesador);
        mem0 = new MEM();
        wb0 = new WB();
    } // fin constructor

    /* Métodos Consultores */
    
    // Devuelve el conjunto de registros de propósito general del núcleo
    public int[] getRegistros() {
        return this.r;
    }
    
    // retorna verdadero si el núcleo está pausado y falso si no
    public boolean getPausa(){
        return this.pausa;
    }
    
    
    // Devuleve verdadero si el núclueo está ejecutando algún hilo y falso si no
    public boolean getEjecutando() {
        return this.ejecutando;
    }
    
    // Devuelve el valor del registro RL
    public int getRL(){
        return this.rl;
    } // fin de getRL
    
    // retorna una hilera con los valores del conjunto de registros de propósito general
    public String volcarRegistros() {
        String volcado = "";
        for (int i = 0; i < r.length; i++) {
            volcado += "R" + i + " -> [" + r[i] + "]\n";
        }
        return volcado;
    } // fin de volcarRegistros
    
    /* Métodos Modificadores */
    
    // Remplaza los registros de uso general por los del vector fuente del parámetro
    public void remplazarRegistros(int[] vecFuente){
        System.arraycopy(vecFuente, 0, this.r, 0, 32);
    } // fin de remplazarRegistros
    
    // Modifica el valor del registro RL
    public void setRL(int reg){
        this.rl = reg;
    } // fin de setRL
    
    // modificar el estado del núcleo
    public void setPausa(boolean estado){
        this.pausa = estado;
    }
    
    // pone el núcleo en pausa o espera
    public void pausar(){
        this.pausa = true;
    }
    
    // "despausa" el núcleo
    public void seguir(){
        this.pausa = false;
    }

    
    /* Métodos y clases internas para la ejecución del núcleo */
    
    // inicia la ejecución del núcleo
    public void exec() throws Exception {
        mutexEjecutar.acquire();
        ejecutando = true;
        // poner el quantum completo a la primera etapa
        this.qIF = this.procesador.getPlanificador().getQuantum();
        //aqui se inician todos los hilos ya listos para ejecutar
        this.start();
        if0.start();
        id0.start();
        ex0.start();
        mem0.start();
        wb0.start();
        mutexEjecutar.release();
    }
    
    // detiene la ejecución del núcleo
    public void detener() throws Exception {
        mutexEjecutar.acquire();
        ejecutando = false;
        mutexEjecutar.release();
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                mutexEjecutar.acquire();
                if (!ejecutando) {
                    //checkeo del estado de ejecucion (todos los hilos ejecutan el mismo codigo)
                    mutexEjecutar.release();
                    break;
                }
                mutexEjecutar.release();
                if(!pausa){          
                    procesador.getUI().imprimirMemoria(procesador.volcarMemoria(procesador.datos()));
                    procesador.getUI().imprimirRegistros(volcarRegistros());
                    //bloqueo de todos los semaforos entre etapas, asi la etapa "a la derecha" 
                    //usa los registros "a la izquierda" sin necesidad de blqouear semaforos, 
                    //luego cada etapa los libera para que la etapa anterior haga su trabajo
                    semIFID.acquire();
                    semIDEX.acquire();
                    semEXMEM.acquire();
                    semMEMWB.acquire();
                    barreraInicia.await();
                    procesador.getUI().imprimirReloj(procesador.getCicloReloj());
                    barrera.await();
                    procesador.incCicloReloj();
                    // decrementar el quantum de IF acda vez que se comience a ejecutar una nueva instrucción
                    this.qIF--;
                    // Verificar quantum de WB
                    if(this.qWB<=0){ // se acabó el quantum del hilo
                        procesador.getPlanificador().setQuantumHilo(false);//informar al planificador que se acabó el quantum del hilo que se estaba ejecutando
                    }
                    sleep(500);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
        }
    } // fin run

    //Sugerencia: que cada clase este dentro de Nucleo, que hereden de 'Thread' y que tengan un miembro de tipo 'Nucleo' para compartir los recursos pero se puede cambiar perfectamente si alguien encuentra una mejor forma.
    public class IF extends Thread {

        public IF() {
            super("Hilo IF");
        }

        @Override
        public void run() {
            try {
                while (true) {
                    mutexEjecutar.acquire();
                    if (!ejecutando) {
                        mutexEjecutar.release();
                        break;
                    }
                    mutexEjecutar.release();
                    if(!pausa){
                        barreraInicia.await();
                        pcIF = procesador.getPC(); //Por el momento este pc no tiene validacion
                        if(banderaIDIF){
                           pcIF = procesador.getPC(); //Por el momento este pc no tiene validacion
                           int opCode = procesador.getInstr(pcIF);
                           int op1 = procesador.getInstr(pcIF + 1);
                           int op2 = procesador.getInstr(pcIF + 2);
                           int op3 = procesador.getInstr(pcIF + 3);
                           semIFID.acquire();
                           irIF = new Instruccion(opCode, op1, op2, op3);
                           semIFID.release();
                           procesador.incPC(4);
                        }
                        barrera.await();
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    } // fin IF

    public class ID extends Thread {

        public ID() {
            super("Hilo ID");
        }

        @Override
        public void run() {
            try {
                while (true) {
                    mutexEjecutar.acquire();
                    if (!ejecutando) {
                        mutexEjecutar.release();
                        break;
                    }
                    mutexEjecutar.release();
                    if(!pausa){
                         
                        barreraInicia.await();
                        if(banderaEXID){
                           pcID = pcIF;
                           irID = irIF;
                           qID = qIF; // copiar el quantum de la etapa anterior
                           banderaIDIF = true;
                           semIFID.release();
                           if (irID != null && irID.valida()) {
                               semIDEX.acquire();
                               switch (irID.getCodigo()) {
                                   case DADDI:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       imm = irID.getOp3();
                                       break;
                                   case DADD:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       break;
                                   case DSUB:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       break;
                                   case DMUL:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       break;
                                   case DDIV:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       break;
                                   case LL:
                                   case LW:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       imm = irID.getOp3();
                                       break;
                                   case SC:
                                   case SW:
                                       a = r[irID.getOp1()];
                                       b = r[irID.getOp2()];
                                       imm = irID.getOp3();
                                       break;
                                   case BEQZ:
                                       //bloquea el hilo IF
                                       banderaIDIF = false;
                                       if(r[irID.getOp1()] == 0){
                                          npc = r[irID.getOp1()] + irID.getOp3();
                                       }
                                       banderaIDIF = true;
                                   break;
                                   case BNEZ:
                                       //bloquea el hilo IF
                                       banderaIDIF = false;
                                       if(r[irID.getOp1()] != 0){
                                          npc = r[irID.getOp1()] + irID.getOp3();
                                       }
                                       banderaIDIF = true;
                                   break;
                                   case JAL:
                                       //bloquea el hilo IF
                                       banderaIDIF = false;
                                       r[31] = pcID;
                                       pcIF = pcID + irID.getOp3();
                                       banderaIDIF = true;
                                   break;
                                   case JR:
                                       //bloquea el hilo IF
                                       banderaIDIF = false;
                                       pcIF = r[irID.getOp1()];
                                       banderaIDIF = true;
                                   break;
                               }
                               semIDEX.release();
                           }
                        }
                        else{
                           banderaIDIF = false;
                        }
                        barrera.await();   
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    } // fin ID

    public class EX extends Thread {
        
        Mips procesador; // procesador

        public EX(Mips p) {
            super("Hilo EX");
            procesador = p;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    mutexEjecutar.acquire();
                    if (!ejecutando) {
                        mutexEjecutar.release();
                        break;
                    }
                    mutexEjecutar.release();
                    if(!pausa){
                        barreraInicia.await();
                        qEX = qID; // copiar el quantum de la etapa anterior
                        if(banderaMEMEX){
                           irEX = irID;
                           banderaEXID = true;
                           if (irEX != null && irEX.valida()) {
                               semEXMEM.acquire();
                               aEX = a;
                               bEX = b;
                               switch (irEX.getCodigo()) {
                                   case DADDI:
                                       ao = aEX + imm;
                                       break;
                                   case DADD:
                                       ao = aEX + bEX;
                                       bEX = r[irEX.getOp3()];
                                       break;
                                   case DSUB:
                                       ao = aEX - bEX;
                                       bEX = r[irEX.getOp3()];
                                       break;
                                   case DMUL:
                                       ao = aEX * bEX;
                                       bEX = r[irEX.getOp3()];
                                       break;
                                   case DDIV:
                                       ao = aEX / bEX;
                                       bEX = r[irEX.getOp3()];
                                       break;
                                   case LL:
                                   case LW:
                                       ao = aEX + imm;
                                       break;
                                   case SC:
                                   case SW:
                                       ao = r[aEX] + imm;
                                       break;
                               }
                               semEXMEM.release();
                           }
                        }
                        else{
                           banderaEXID = false;
                        }
                        semIDEX.release();
                        barrera.await();   
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    } // fin EX

    public class MEM extends Thread {

        public MEM() {
            super("Hilo MEM");
        }

        @Override
        public void run() {
            try {
                while (true) {
                    mutexEjecutar.acquire();
                    if (!ejecutando) {
                        mutexEjecutar.release();
                        break;
                    }
                    mutexEjecutar.release();
                    if(!pausa){
                        barreraInicia.await();
                        qMEM = qEX; // copiar el quantum de la etapa anterior
                        if(contador == 0){
                           irMEM = irEX;
                           banderaMEMEX = true;
                           contador = 0;
                           aMEM = aEX;
                           bMEM = bEX;
                           aoMEM = ao;
                           boolean fallo;
                           ResultadoCache r;
                           if (irMEM != null && irMEM.valida()) {
                               semMEMWB.acquire();
                               switch (irMEM.getCodigo()) {
                                   case LL:
                                       direccionLL = aoMEM;
                                   case LW:
                                       r = memCache.getPalabra(aoMEM);
                                       if(r.falloCache){
                                          contador = 48;
                                          banderaMEMEX = false;
                                       }
                                       else{
                                          lmd = r.palabra;
                                       }
                                       break;
                                   case SC:
                                       direccionSC = aoMEM;
                                       if (rl == direccionSC) {
                                           falloSC = false;
                                           fallo = memCache.setPalabra(aoMEM, bMEM);
                                            if(fallo){
                                               contador = 48;
                                               banderaMEMEX = false;
                                            }
                                       } else {
                                           falloSC = true;
                                       }
                                       break;
                                   case SW:
                                       fallo = memCache.setPalabra(aoMEM, bMEM);
                                       if(fallo){
                                          contador = 48;
                                          banderaMEMEX = false;
                                       }
                                       break;
                               }
                               semMEMWB.release();
                           }
                        }
                        else{
                           --contador;
                        }
                        semEXMEM.release();
                        barrera.await();   
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    } // fin MEM

    public class WB extends Thread {

        public WB() {
            super("Hilo WB");
        }

        @Override
        public void run() {
            try {
                while (true) {
                    mutexEjecutar.acquire();
                    if (!ejecutando) {
                        mutexEjecutar.release();
                        break;
                    }
                    mutexEjecutar.release();
                    if(!pausa){
                        barreraInicia.await();
                        qWB = qMEM; // copiar el quantum de la etapa anterior
                        irWB = irMEM;
                        aWB = aMEM;
                        bWB = bMEM;
                        aoWB = aoMEM;
                        if (irWB != null && irWB.valida()) {
                            switch (irWB.getCodigo()) {
                                case DADDI:
                                case DADD:
                                case DSUB:
                                case DMUL:
                                case DDIV:
                                    r[bWB] = aoWB;
                                    break;
                                case LL:
                                    rl = direccionLL; //RL contiene la direccion de memoria obtenida en EX
                                case LW:
                                    r[bWB] = lmd;
                                    break;
                                case SC:
                                    if (falloSC) {
                                        r[aWB] = 0;
                                    }
                                case FIN:
                                    procesador.getPlanificador().setFinHilo(true);
                                    ejecutando = false;
                                    break;
                            }
                        }
                        semMEMWB.release();
                        barrera.await();   
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    } // fin WB

} // fin de la clase Nucleo
