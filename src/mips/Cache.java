/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mips;

/**
 * Clase cache para usar en un nucleo mips
 *
 * @author a96270
 */
class Cache {
	public static final int BLOQUE_VALIDO_COMPARTIDO = 0;
	public static final int BLOQUE_VALIDO_MODIFICADO = 1;
	public static final int BLOQUE_INVALIDO = -1;
	public static final int TAMANO_PALABRA = 4;
	private Memoria memArray;
	private int[][] mCache;
	private int palabrasPorBloque;

	/**
	 * Constructor de Cache usando una referencia a un vector de memoria
	 * principal
	 *
	 * @param mem Referencia a un vector de datos que será guardado en cache
	 * @param numBloques Numero de bloques que puede guardar la cache
	 * @param palabrasPorBloque Numero de palabras (enteros) que puede guardar
	 * cada bloque
	 */
	public Cache(Memoria mem, int numBloques, int palabrasPorBloque) {
		memArray = mem;
		this.palabrasPorBloque = palabrasPorBloque;
		int tCache = palabrasPorBloque + 2;
		mCache = new int[numBloques][tCache]; //dos campos extra para valido y para etiqueta
		for (int i = 0; i < numBloques; i++) { //llenar de -1 la etiqueta
			mCache[i][tCache - 1] = -1;
			mCache[i][tCache - 2] = BLOQUE_INVALIDO;
		}
	}

	/**
	 * Obtiene una entero correspondiente a una palabra en memoria cache,
	 * tambien devuelve un booleano indicando si hubo fallo de cache
	 *
	 * @param direccion direccion fisica de la palabra
	 * @return un ResultadoCache que contiene el entero obtenido de cache y un
	 * booleano que indica si hubo fallo de cache
	 */
	public ResultadoCache getPalabra(int direccion) {
		DireccionCache direccionCache = mappearDireccion(direccion);
		int pos = direccionCache.bloque % mCache.length; //mappeo directo de la direccion
		boolean fallo = mCache[pos][palabrasPorBloque + 1] != direccionCache.bloque || mCache[pos][palabrasPorBloque] == BLOQUE_INVALIDO; //si la etiqueta no coincide o el bloque esta marcado como invalido
		if (fallo) {//fallo de cache
			reemplazarBloque(pos, direccionCache.bloque);
		}
		int valor = mCache[pos][direccionCache.palabra];
		ResultadoCache r = new ResultadoCache(valor, fallo);
		return r;
	}

	/**
	 * Setea el valor de una palabra en memoria cache y deja el bloque completo
	 * en cache si este no estaba cargado, si hubo fallo de cache el bloque
	 * reemplazado se guarda en memoria
	 *
	 * @param direccion direccion de la palabra a modificar
	 * @param valor valor nuevo de dicha palabra
	 * @return devuelve true si el bloque estaba en cache y false si hubo fallo
	 * de cache o se dio una direccion invalida
	 */
	public boolean setPalabra(int direccion, int valor) {
		DireccionCache direccionCache = mappearDireccion(direccion);
		int pos = direccionCache.bloque % mCache.length;
		boolean fallo = mCache[pos][palabrasPorBloque + 1] != direccionCache.bloque || mCache[pos][palabrasPorBloque] == BLOQUE_INVALIDO;
		if (fallo) {
			reemplazarBloque(pos, direccionCache.bloque);
		}
		mCache[pos][direccionCache.palabra] = valor;
		mCache[pos][palabrasPorBloque] = BLOQUE_VALIDO_MODIFICADO;
		return fallo;
	}

	private DireccionCache mappearDireccion(int direccion) {
		if (direccion < 0 || (direccion % 4 != 0)) {
			return null; //si la direccion no es multiplo de 4 o es negativa
		}
		int bloque = direccion / (TAMANO_PALABRA * (mCache[0].length - 2));
		int palabra = (direccion % (TAMANO_PALABRA * (mCache[0].length - 2))) / (mCache[0].length - 2);
		DireccionCache posicion = new DireccionCache(bloque, palabra);
		return posicion;
	}

	private void reemplazarBloque(int posicion, int nuevobloque) {
		if (posicion < 0 || posicion >= mCache.length || nuevobloque < 0) {
			return;
		}
		int numBloque = mCache[posicion][palabrasPorBloque + 1];
		int estadoBloque = mCache[posicion][palabrasPorBloque];
		int fisica = nuevobloque * palabrasPorBloque * TAMANO_PALABRA;
		//escribir a memoria solo si no es un bloque invalido
		if (estadoBloque != BLOQUE_INVALIDO && numBloque >= 0) {
			int direccionDestiono = numBloque * palabrasPorBloque * TAMANO_PALABRA;
			try {
				for (int i = 0; i < palabrasPorBloque; i++) {
					memArray.set(direccionDestiono + i*4, mCache[posicion][i]);
				}
			} catch (IllegalArgumentException e) {
				return;
			}
		}
		//subir el bloque indicado a cache y poner el estado en compartido
		try {
			for (int i = 0; i < palabrasPorBloque; i++) {
				mCache[posicion][i] = memArray.get(fisica + i*4);
			}
		} catch (IllegalArgumentException e) {
			return;
		}
		mCache[posicion][palabrasPorBloque + 1] = nuevobloque;
		mCache[posicion][palabrasPorBloque] = BLOQUE_VALIDO_COMPARTIDO;
	}

	/**
	 * Clase utilizada para devolver resultado de memoria Cache
	 */
	public static class ResultadoCache {

		public final boolean falloCache;
		public final int palabra;

		/**
		 * Construcot de ResultadoCache
		 *
		 * @param valor el entero obtenido de Cache
		 * @param fallo booleano que indica si hubo fallo de cache o no
		 */
		public ResultadoCache(int valor, boolean fallo) {
			falloCache = fallo;
			palabra = valor;
		}
	}

	/**
	 * Estructura usada para devolver una direccion fisica mappeada a direccion
	 * en cache
	 */
	public static class DireccionCache {

		public final int bloque;
		public final int palabra;

		public DireccionCache(int bloque, int palabra) {
			this.bloque = bloque;
			this.palabra = palabra;
		}
	}
}
