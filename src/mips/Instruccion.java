/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mips;

/**
 * Simple estructura de datos para ir pasando las instrucciones en el pipeline
 * @author Heynner
 */
public class Instruccion {
	//Codigos de operacion
	public static final int DADDI = 8;
	public static final int DADD = 32;
	public static final int DSUB = 34;
	public static final int DMUL = 12;
	public static final int DDIV = 14;
	public static final int LW = 35;
	public static final int SW = 43;
	public static final int BEQZ = 4;
	public static final int BNEZ = 5;
        public static final int LL = 50;
        public static final int SC = 51;
	public static final int JAL = 3;
	public static final int JR = 2;
	public static final int FIN = 63;
	
	private int codigo;//codigo de operacion
	
	//Operandos, de acuerdo al tipo de instruccion algunos podrian no utilizarse
	private int op1;//Operando 1 (segun el tipo de instruccion cambia el significado)
	private int op2;//Operando 1 (segun el tipo de instruccion cambia el significado)
	private int op3;//Operando 1 (segun el tipo de instruccion cambia el significado)
	
	boolean valido; //Variable que indica si la instruccion es valida (codificacion, tipo de operandos, etc)
	
	public Instruccion(int opCode, int o1, int o2, int o3) {
		codigo = opCode;
		op1 = o1;
		op2 = o2;
		op3 = o3;
		switch (codigo) {
			case DADDI:
			case DADD:
			case DSUB:
			case DMUL:
			case DDIV:
			case LW:
			case SW:
			case BEQZ:
			case BNEZ:
                        case LL:
                        case SC:
			case JAL:
			case JR:
			case FIN:
				valido = true;
				break;
			default:
				valido = false;
		} //validacion provicional de las instrucciones
		//usado para detener la ejecucion del mips en caso de instrucciones invalidas
	}
	
	//Datos read-only
	public int getCodigo() {
		return codigo;
	}
	
	public int getOp1() {
		return op1;
	}
	
	public int getOp2() {
		return op2;
	}
	
	public int getOp3() {
		return op3;
	}
	
	public boolean valida() {
		return valido;
	}
}
