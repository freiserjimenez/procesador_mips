/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mips;

/**
 * Estructura de datos que representa el contexto de un hilo en el procesador MIPS
 * @author freiser
 */
public class Hilo {
    // Atributos
    private int id; // identificador del hilo
    private int rl; // valor del registro RL del contexto del hilo
    private int[] r; // valores del conjunto de registros enteros de propósito genereal del contexto del hilo
    private int pc; // valor del contador del programa del cotexto del hilo
        
    // Contrutor
    public Hilo(int identificador, int programCounter){
        this.id = identificador; // cada hilo se inicializa con el consecutivo en el VI
        this.rl = -1; // siempre se inicializa el rl con -1
        this.r = new int[32]; // cuando se carga un hilo siempre viene con sus registros en blanco
        this.pc = programCounter; // al cargarse el hilo se debe poner como su pc su pos inicial en el VI
    } // fin del constructor
    
    /* Métodos Consultores */
    
    // retorna el id del hilo
    public int getID(){
        return this.id;
    } // fin getID
    
    // retorna el registro rl del hilo
    public int getRL(){
        return this.rl;
    } // fin de getRL
    
    // retorna el vector de registros del hilo
    public int[] getRegistros() {
        return this.r;
    } // fin de getRegistros
    
    // retorna el pc del hilo
    public int getPC(){
        return this.pc;
    } // fin de getPC
    
    
    /* Métodos Modificadores */
    
    // modifica el identificador del hilo
    public void setID(int identificador){
        this.id = identificador;
    } // fin setID
    
    // modifica el registro RL del hilo
    public void setRL(int regRL){
        this.rl = regRL;
    } // fin de setRL
    
    // copia los valores de los registros del vector parámetro vecFuente al vector de registros r del hilo
    public void copiarRegistros(int[] vecFuente){
        System.arraycopy(vecFuente, 0, this.r, 0, 32);
    } // fin de copiarR
    
    // modifica el valor del pc del hilo
    public void setPC(int programCounter){
        this.pc = programCounter;
    } // fin de setPC
    
} // fin de la clase Hilo
