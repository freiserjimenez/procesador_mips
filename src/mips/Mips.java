/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mips;
import com.sun.nio.sctp.IllegalReceiveException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 
 */
public class Mips {
    //private int[] memDatos; // Vector de memoria de datos (enteros)
    private Memoria memDatos;
    private int[] memInstr; // Vector de memoria de instrucciones
    private int reloj; // reloj del procesador
    private int pc; // program counter
    private Nucleo n0; // Nucleo 0 del procesador
    private Interfaz ui;
    private Planificador plan; // planificador de hilos
    private int pcInicioHilo; // indica el inicio de un hilo en el VI
    private int posMem; // contador de la última posición de memoria donde se carga una instrucción
    private int numHilos; // contador de hilos cargados en memoria

    // Constructor que recibe el tamano de las dos memorias
    // tDatos (tamaño vector datos) debe ser cualquier entero positivo
    // tInstr (tamaño vector instrucciones) debe ser un entero positivo multiplo de 4
    public Mips(int tDatos, int tInstr) throws Exception {
        if (tDatos < 1) throw new Exception("El vector de datos debe ser de tamano mayor que 0");
        if (tInstr < 1 || (tInstr % 4) != 0) throw new Exception("El vector de instrucciones debe ser de tamano positivo multiplo de 4");

        // El desplazamiento se hace con el base en el tamano de la memoria de instrucciones
        this.memDatos = new Memoria(tDatos, tInstr); // el tamaño del vector de datos debe ser mínimo 200
        this.memInstr = new int[tInstr]; // el tamaño del vector de instrucciones debe ser mínimo 400
        this.reloj = 1; // inicializar el reloj del procesador
        this.pc = 0; // inicializar el pc del proceador
        this.pcInicioHilo = 0; // inicializar el pc del primer hilo
        this.posMem = 0; // inicializar el contador de posciciones de memoria cargadas con instrucciones
        this.numHilos = 0; // inicializar el contador de hilos, esto sirve para poner el id de cada hilo
        this.n0 = new Nucleo(this); // inicializar el núcleo 0 del procesador
        this.ui = new Interfaz(this); // inicializar la interfaz
        this.plan = new Planificador(this); // inicailizar el planificador del procesador
    } // fin del constructor


    /* Métodos Consultores */

    // retorna la interfaz de usuario
    public Interfaz getUI() {
        return this.ui;
    }

    // retorna el nucleo # 0 del procesador
    public Nucleo getNucleo(){
        return this.n0;
    }

    //gets y sets para la memoria, lo estoy haciendo asi por que java no tiene algo como friend class, si alguien lo puede mejorar estaria bien
    public int getDato(int i) throws IllegalArgumentException {
            return this.memDatos.get(i);
    }

    public int getInstr(int i) throws ArrayIndexOutOfBoundsException {
            return this.memInstr[i];
    }

    // obtener el ciclo actual del reloj
    public int getCicloReloj(){
        return this.reloj;
    }

    //obtener todo el arreglo de datos
    public int[] datos() {
            int x = this.memDatos.tamano()/4;
            int[] datos = new int[x];
			int d = memDatos.desplazamiento();
            for (int i = 0; i < x; i++) {
                datos[i] = this.memDatos.get(i*4+d);
            }
            return datos;
    }

     // retorna la memoria de datos
    public Memoria getMemoriaDatos() {
        return this.memDatos;
    }

    public int[] instrucciones() {
            return this.memInstr;
    }

    // retorna el valor del pc del procesador
    public int getPC(){
        return this.pc;
    }

    // retornar una hilera de caracteres con el valor actual del reloj
    public String relojToString(){
        return this.reloj+"";
    }

    // retorna el objeto planificador
    public Planificador getPlanificador(){
        return this.plan;
    }

    // retorna una hilera de caracteres con las direcciones y valores de la memoria pasada por parámetro
    public String volcarMemoria(int[] mem){
        String direccion;
        String datos = "";
        String volcado = "";

        for(int i=0; i<mem.length; i++){
            if((i%4) == 0){ // si el indice es multiplo de 4
                for(int j=i; j<i+4; j++){
                    datos += "[" + mem[j] + "]";
                }
                direccion = i+"x0000";
                volcado += direccion + "->" + datos + "\n";
                datos="";
            }
        }

        return volcado;
    } // fin de volcarMemoria

    /* Métodos Modificadores */

    public void setDato(int i, int v) throws ArrayIndexOutOfBoundsException {
            this.memDatos.set(i, v);
    }


    public void setInstr(int i, int v) throws ArrayIndexOutOfBoundsException {
            this.memInstr[i] = v;
    }

    // cambia el valor del pc al valor del parámetro val
    public void setPC(int val){
        this.pc = val;
    }

    // incrementa el valor del pc en uno y retorna el resultado
    // Se incrementa el pc de acuerdo a "inc" (util para jumps
    public int incPC(int inc){
        this.pc += inc;
        return this.pc;
    }

    // decrementa el valor del pc en uno y retorna el resultado
    // podria elimarse este método ya que incPC acepta valores negativos
    public int decPC(){
        this.pc = this.pc-1;
        return this.pc;
    }      

    // modificar el ciclo del reloj
    public void setCicloReloj(int ciclo){
        this.reloj = ciclo;
    }

    // incrementa el ciclo del reloj
    public void incCicloReloj(){
        this.reloj++;
    }

    // Restablece el ciclo del reloj a cero
    public void resetReloj(){
        this.reloj = 0;
    }

    // cargar el programa en la memoria y encolar el hilo en el planificador
    public void cargarHilo(File archivo){
        //cargar archivo a memoria
        String linea = "";
        String[] temp;
        try{
            Scanner entrada = new Scanner(archivo);
            do{
                linea = entrada.nextLine();
                temp = linea.split(" ");
                try{
                    this.memInstr[this.posMem] = Integer.parseInt(temp[0]);
                    ++this.posMem;
                    this.memInstr[this.posMem] = Integer.parseInt(temp[1]);
                    ++this.posMem;
                    this.memInstr[this.posMem] = Integer.parseInt(temp[2]);
                    ++this.posMem;
                    this.memInstr[this.posMem] = Integer.parseInt(temp[3]);
                    ++this.posMem;
                }catch(Exception e){
                    Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
                }
            }while(entrada.hasNextLine());

            entrada.close();
            // crear el hilo para que lo pueda usar el planificador
            Hilo h = new Hilo(this.numHilos, this.pcInicioHilo);
            // encolar el nuevo hilo en el planificador de hilos
            if(this.plan.encolarHilo(h)){ 
                System.out.println("Hilo "+numHilos+" cargado a memoria y encolado al planificador");
            }else{
                System.out.println("error encolando hilo "+numHilos);
            }
            this.numHilos++; // incrementar el contador de hilos cargados
            this.pcInicioHilo = this.posMem; // incrementar el pc para que haga referencia el pc del proximo hilo
        }catch(Exception e){
            Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
        }
    } // fin de cargarHilo

    // manda a ejecutar el programa cargado
    public void ejecutarPrograma(){
        if(plan.colaVacia()){
            System.out.println("¡Ningún hilo cargado, nada por hacer!");
        }else{
            System.out.println("Inicio ejecución de "+plan.numHilosEncolados()+" hilos");
            try {
                this.plan.exec(); // iniciar la planificación de hilos
            } catch (Exception ex) {
                Logger.getLogger(Mips.class.getName()).log(Level.SEVERE, null, ex);
            }                
        }
    } // fin de ejecutarPrograma

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Mips m1 = new Mips(4000,768);
            
        } catch (Exception e) {
            Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, e);
        }
        
    } // fin del método main
    
} // fin de la clase Mips
