/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mips;

/**
 * Clase utilizada para manejar la memoria mappeando el desplazamiento logico al vector "fisico" de memoria
 * @author Heynner
 */
public class Memoria {
	int[] mem;
	int mDesp; //para manipular la memoria de cualquier forma el desplazamiento se resta a la direccion dada para obtener el indice en la estructura de datos usada
	
	/**
	 * Crea una estructura de datos para manejar memoria principal de un Mips.
	 * <p>
	 * El tamaño debe ser múltiplo de 4, internamente se crea un vector de tamaño tamano/4 para manipular la palabra completa (leer o escribir)
	 * @param tamano tamaño físico de la memoria, positivo y múltiplo de 4
	 * @param desplazamiento cantidad de bytes hacia el cual se desplaza el inicio de esta memoria
	 * @throws IllegalArgumentException si el tamaño o el desplazamiento son menores a 0 o no son múltiplos de 4
	 */
	public Memoria(int tamano, int desplazamiento) throws IllegalArgumentException {
		if (tamano < 0 || (tamano % 4) != 0) throw new IllegalArgumentException("El tamaño de la memoria debe ser multiplo de 4 y mayor que 0");
		if (desplazamiento < 0 || (desplazamiento % 4) != 0) throw new IllegalArgumentException("El desplazamiento debe ser múltiplo de 4 y mayor a 0");
		mem = new int[tamano/4];
		mDesp = desplazamiento;
	}
	
	public int get(int i) throws IllegalArgumentException {
		if ((i % 4) != 0) throw new IllegalArgumentException("La direccion debe ser multiplo de 4");
		int direccion = (i-mDesp)/4; //convertir a direccion interna del vector y devolver la palabra completa
		return mem[direccion];
	}
	
	public void set(int i, int val) throws IllegalArgumentException {
		if ((i % 4) != 0 || i < 0) throw new IllegalArgumentException("La direccion debe ser multiplo de 4");
		int direccion = (i-mDesp)/4;
		mem[direccion] = val;
	}
	
	public int tamano() {
		return mem.length*4;
	}
	
	public int desplazamiento() {
		return mDesp;
	}
}
