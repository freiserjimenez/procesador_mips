/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mips;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Algoritmo de planificación RR, el cual usa una cola circular de hilos como estructura. 
 * El planiﬁcador la recorre asignando un cuanto de tiempo a cada proceso. 
 * La organización de la cola es FIFO. 
 * El Quantum se suele implantar mediante un temporizador que genera una interrupción cuando se agota el Quantum de tiempo. 
 * Si el hilo agota su ráfaga de CPU antes de ﬁnalizar el Quantum, el planiﬁcador asigna la CPU inmediatamente a otro hilo.
 * @author freiser
 */
public class Planificador extends Thread {
    
    // Atributos
    private Queue<Hilo> cola; // cola de contextos de hilos
    private int quantum; // ciclos de CPU para cada hilo 
    private boolean finHilo; // verdadero si el hilo ya finalizó
    private boolean quantumHilo; // Verdadero si el hilo tiene quantum
    private Mips procesador; // procesador
    private Hilo h;
    
    // Constructor por defecto
    public Planificador(){
        super("Hilo del planificador");
        this.quantum = 1;
        this.cola = new LinkedList<Hilo>();
        this.procesador = null;
    } // fin del contructor vacío
    
    // Constructor con quantum
    public Planificador(int qtm){
        super("Hilo del planificador");
        this.quantum = qtm;
        this.cola = new LinkedList<Hilo>();
        this.procesador = null;
    } // fin del contructor por quantum
    
    // Constructor con procesador
    public Planificador(Mips pro){
        super("Hilo del planificador");
        this.quantum = 1;
        this.cola = new LinkedList<Hilo>();
        this.procesador = pro;
    } // fin del contructor por procesador
    
    // Constructor con quantum y procesador
    public Planificador(int qtm, Mips pro){
        super("Hilo del planificador");
        this.quantum = qtm;
        this.cola = new LinkedList<Hilo>();
        this.procesador = pro;
    } // fin del contructorquantum y procesador
    
    /* Métodos Consultores */
    
    public Hilo getH(){
        return this.h;
    }
    
    // Retorna verdadero si el hilo en ejecución aún le queda quantum y falso si no
    public boolean getQuantumHilo(){
        return this.quantumHilo;
    } // fin de qHilo
    
    // Retorna el quantum del planificador
    public int getQuantum(){
        return this.quantum;
    } // fin de getQuantum
    
    // Retorna la cola de hilos del Planificador
    public Queue<Hilo> getCola(){
        return this.cola;
    } // fin de getCola
    
    // retorna el procesador del planificador
    public Mips getProcesador(){
        return this.procesador;
    } // fin de getProcesador
    
    // retorna verdadero si el hila ya finalizó y falso si no
    public boolean getFinHilo(){
        return this.finHilo;
    }
    
    /* Métodos Modificadores */
    
    public void setH(Hilo elH){
        this.h = elH;
    }
    
    // Modifica la bandera que indica que el hilo tiene quantum disponible
    public void setQuantumHilo(boolean estado){
        this.quantumHilo = estado;
    } // fin setQHilo
    
    // Modifica el quentum del planificador con el valor del parámetro
    public void setQuantum(int q){
        this.quantum = q;
    } // fin de setQuantum
    
    // Modifica la cola de hilos del Planificador
    public void setCola(Queue<Hilo> c){
        this.cola = c;
    } // fin de setCola
    
    // Modifica el procesador
    public void setProcesador(Mips pro){
        this.procesador = pro;
    } // fin setProcesador
    
    // modifica el estado de finalización del hilo en ejecución
    public void setFinHilo(boolean estado){
        this.finHilo = estado;
    }
        
    
    /* Métodos para manejar la cola de hilos */
    
    // Retorna verdadero si se logra encolar un objeto tipo Hilo, de otra forma retorna falso
    public boolean encolarHilo(Hilo h){
        return this.cola.offer(h);
    } // fin de encolarHilo
    
    // Desencola, remueve y retorna un objeto tipo Hilo de de la cola
    public Hilo desencolarHilo(){
        return this.cola.poll();
    } // fin de desencolarHilo
    
    // retorna verdadero si la cola está vacía y falso si no
    public boolean colaVacia(){
        return this.cola.isEmpty();
    } // fin colaVacia
    
    // retorna el número de hilos encolados
    public int numHilosEncolados(){
        return this.cola.size();
    } // fin de numHilosEncolados
    
    /* Métodos para planificar los hilos */
    
    // iniciar la ejecución
    public void exec() throws Exception {
        this.start();
    } // fin de excec
    
    // comienza la planificación de los hilos
    @Override
    public void run() {
        
        while(!this.cola.isEmpty()){ // mientras hayan hilos encolados
            
            // desencolar hilo y asignarle cpu
            Hilo h = this.desencolarHilo();
            procesador.setPC(h.getPC());
            procesador.getNucleo().remplazarRegistros(h.getRegistros());
            procesador.getNucleo().setRL(h.getRL());
            System.out.println("desencolando hilo "+h.getID());
            this.setFinHilo(false);
            this.setQuantumHilo(true);
            
            // iniciar ejecución del hilo en el núcleo
            //procesador.getNucleo().setPausa(false);
            if(!procesador.getNucleo().getEjecutando()){
                try {
                    procesador.getNucleo().exec();
                } catch (Exception ex) {
                    Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }                        
            
            procesador.getNucleo().seguir();
            
            // mientras el hilo tenga quantum
            while(this.getQuantumHilo()){
                // esperar a que se acabe el quantum
            }
               
            // Pausar núcleo
            procesador.getNucleo().pausar();
            
            // Analizar el estado del hilo
            if(!this.getFinHilo()){ // si el hilo no logró finalizar
                // guardar el contexto del hilo
                h.setPC(procesador.getPC());
                h.setRL(-1);
                h.copiarRegistros(this.procesador.getNucleo().getRegistros());
                // volver a encolar el hilo
                this.encolarHilo(h);
            }else{
                // si el hilo logró finalizar, dejarlo fuera de la cola
            }
            
            /*Hilo h = this.desencolarHilo();
            this.setH(h);
            //System.out.println("desencolando hilo "+h.getID());
            procesador.getNF().setQuantum(this.quantum);
            
            //procesador.getNF().seguir();
                        
            // iniciar ejecución del hilo en el núcleo
            if(!procesador.getNF().getEjecutando()){
                try {
                    procesador.getNF().exec();
                } catch (Exception ex) {
                    Logger.getLogger(Planificador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            procesador.getNF().seguir();

            // mientras el hilo tenga quantum
            while(this.getQuantumHilo()){
                // esperar a que se acabe el quantum
            }
            
            procesador.getNF().pausar();
           
            // Analizar el estado del hilo
            if(!this.getFinHilo()){ // si el hilo no logró finalizar
               // guardar el contexto del hilo
               this.encolarHilo(h);
            }else{
                // si el hilo logró finalizar, dejarlo fuera de la cola
            }*/
            
        } // fin del ciclo principal
        
        System.out.println("Programa Finalizado");
                
    } // fin de run
    
} // fin de la clase Planificador